## To run the App:

### npm i

### npm start

After that, the application will start running and will try to retrieve a REACT_APP_APPID key for OpenWeatherApi from your env, in case it's not available, it will use my free key.

## Project

This project was developed using ReactJS along with Material-UI just to give a simple interface and React-Vis to create a simple Line Chart to visualize some data.

I left the APPID for the OpenWeatherApi just for a simplified run and play, but it's recommended to use your own key.
